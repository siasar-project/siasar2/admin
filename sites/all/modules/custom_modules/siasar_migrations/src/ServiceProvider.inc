<?php

/**
 * Migration for PAT entityforms.
 */
class ServiceProvider extends SiasarMigrationBase {
  public function __construct($arguments = []) {
    // Set entity form type ID.
    $this->setBundleId('prestador_de_servicio');

    // Create field maps.
    $this->addFieldWrappers([
      new FieldCountryWrapper('field_pais'),
      new FieldDateWrapper('field_fecha_aplicacion'),
      new FieldWorkflowWrapper('field_status'),
      new FieldEntityReferenceWrapper('field_user_reference'),
      new FieldImageReferenceWrapper('field_imagenps'),
      new FieldTextWrapper('field_entity_name'),
      new FieldGeofieldWrapper('field_latitud'),
      new FieldDecimalWrapper('field_altitud'),
      new FieldTextWrapper('field_codigo_de_prestador'),
      new FieldTermReferenceWrapper('field_clase_de_prestador'),
      new FieldTextWrapper('field_otra_clase_de_prestador'),
      new FieldDateWrapper('field_fecha_creacion'),
      new FieldTermReferenceWrapper('field_estado_legal'),
      new FieldDateWrapper('field_fecha_eleccion'),
      new FieldBooleanWrapper('field_miembros_nombrados'),
      new FieldIntegerWrapper('field_numero_reuniones'),
      new FieldBooleanWrapper('field_cuenta_banco'),
      new FieldBooleanWrapper('field_rinde_cuentas'),
      new FieldBooleanWrapper('field_acta_rendicion_cuentas'),
      new FieldBooleanWrapper('field_tiene_definido_tipo_tarifa'),
      new FieldTermReferenceWrapper('field_tipo_de_tarifa'),
      new FieldMeasureWrapper('field_tarifa_media_mensual'),
      new FieldTermReferenceWrapper('field_mecanismo_pago_tarifa', TRUE),
      new FieldTextWrapper('field_otros_motivos'),
      new FieldBooleanWrapper('field_inf_medicion_agua'),
      new FieldMeasureWrapper('field_agua_producida'),
      new FieldMeasureWrapper('field_agua_facturada'),
      new FieldIntegerWrapper('field_usuarios_deberian_pagan'),
      new FieldMeasureWrapper('field_facturacion_promedio'),
      new FieldIntegerWrapper('field_usuarios_al_dia'),
      new FieldMeasureWrapper('field_ingresos_reales'),
      new FieldTermReferenceWrapper('field_mantenimiento_sistema', TRUE),
      new FieldTextWrapper('field_otros_mantenimientos'),
      new FieldBooleanWrapper('field_ingresos_extra'),
      new FieldMeasureWrapper('field_monto_total_finalizado'),
      new FieldMeasureWrapper('field_monto_previsto'),
      new FieldBooleanWrapper('field__aportaciones_extras'),
      new FieldMeasureWrapper('field_monto_aporte_finalizado'),
      new FieldMeasureWrapper('field_monto_aporte_previsto'),
      new FieldDecimalWrapper('field_tasa_anual_de_expansion_pr'),
      new FieldMeasureWrapper('field_gasto_promedio_admin'),
      new FieldMeasureWrapper('field_gasto_teorico_admin'	),
      new FieldMeasureWrapper('field_gasto_real_operacion'	),
      new FieldMeasureWrapper('field_gasto_teorico_operacion'	),
      new FieldMeasureWrapper('field_gasto_real_mantenimiento'	),
      new FieldMeasureWrapper('field_gasto_teorico_mantenimient'	),
      new FieldMeasureWrapper('field_gasto_real_ambientales'	),
      new FieldMeasureWrapper('field_gasto_teorico_ambientales'	),
      new FieldMeasureWrapper('field_sumatoria_gastos_reales'	),
      new FieldMeasureWrapper('field_sumatoria_gastos_teoricos'	),
      new FieldBooleanWrapper('field_libro_ingresos_egresos'),
      new FieldMeasureWrapper('field_monto_total_de_ingresos'	),
      new FieldMeasureWrapper('field_monto_total_de_egresos'	),
      new FieldBooleanWrapper('field__cuenta_con_fondos_disponi'),
      new FieldMeasureWrapper('field_monto_total_fondos'	),
      new FieldBooleanWrapper('field_balance_contable'),
      new FieldMeasureWrapper('field_activos_corrientes'	),
      new FieldMeasureWrapper('field_activos_no_corrientes'	),
      new FieldMeasureWrapper('field_pasivos_corrientes_'	),
      new FieldMeasureWrapper('field_pasivos_no_corrientes'	),
      new FieldTermReferenceWrapper('field_atencion_del_prestador'),
      new FieldBooleanWrapper('field_recursos_prestador'),
      new FieldBooleanWrapper('field_personal_prestador'),
      new FieldTermReferenceWrapper('field_reglamento_servicios'),
      new FieldBooleanWrapper('field_apoyo_gob_otros'),
      new FieldTextWrapper('field_nombre_entidad_apoya'),
      new FieldBooleanWrapper('field_higiene_comunitaria'),
      new FieldBooleanWrapper('field_proteccion_zona_fuente'),
      new FieldTermReferenceWrapper('field_no_uso_plaguicidas'),
      new FieldTermReferenceWrapper('field_no_aguas_residuales'),
      new FieldTermReferenceWrapper('field_reforestacion'),
      new FieldTermReferenceWrapper('field_sustitucion_componentes'),
      new FieldTermReferenceWrapper('field_proteccion_terreno'),
      new FieldTermReferenceWrapper('field_vigilancia_zona'),
      new FieldTermReferenceWrapper('field_proteccion_flora_fauna'),
      new FieldTermReferenceWrapper('field_revision_demarcacion'),
      new FieldTermReferenceWrapper('field_revision_estado_cercano'),
      new FieldTermReferenceWrapper('field_revision_estado_obra'),
      new FieldTermReferenceWrapper('field_revision_componentes_toma'),
      new FieldTermReferenceWrapper('field_no_tala_y_resforestacion'),
      new FieldTermReferenceWrapper('field_proteccion_suelos'),
      new FieldTermReferenceWrapper('field_revision_plan_contingencia'),
      new FieldLongTextWrapper('field_comentarios'),
      new FieldEntityReferenceWrapper('field_rating_s1'),
      new FieldTextWrapper('field_data_source'),
      new FieldIntegerWrapper('field_closed_revisions', TRUE),
    ]);

    $local_entity_field = new FieldTermReferenceWrapper('field_entidad_local');
    $local_entity_field->setSubmigration('tid', 'AdministrativeDivision');

    $this->addFieldWrapper($local_entity_field);

    // Set dependencies.
    $this->dependencies = ['Files', 'AdministrativeDivision', 'Users'];

    // Prepare migration.
    parent::__construct($arguments);
  }

  /**
   * Child class conditions.
   */
  protected function customConditions() {
    // Custom conditions.
    $this->query->condition(FieldBaseWrapper::getDefaultTableAlias('field_pais') . '.' . 'field_pais_iso2', 'KG');
  }

  /**
   * Child class conditions.
   */
  protected function customCountConditions() {
    $countryField = new FieldCountryWrapper('field_pais');
    $countryField->setMigration($this);
    $countryField->addTables($this->count_query);
    // Custom conditions.
    $this->count_query->condition(FieldBaseWrapper::getDefaultTableAlias('field_pais') . '.' . 'field_pais_iso2', 'KG');
  }

}
