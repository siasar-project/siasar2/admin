<?php

/**
 * Migration class for N11 administrative divisions terms.
 */
class AdministrativeDivisionsMigration extends Migration {

  /**
   * {@inheritDoc}
   */
  public function __construct($arguments = []) {
    parent::__construct($arguments);

    $query = Database::getConnection('default', 'legacy')->select('taxonomy_term_data', 'ttd');
    $query->leftJoin('taxonomy_term_hierarchy', 'tth', 'tth.tid = ttd.tid');
    $query->join('field_data_field_pais', 'country', "country.entity_id = ttd.tid AND country.entity_type = 'taxonomy_term'");
    $query->leftJoin('field_data_field_codigo_division_admin', 'cod', "cod.entity_id = ttd.tid AND cod.entity_type = 'taxonomy_term'");
    $query->fields('ttd');
    $query->fields('tth', ['parent']);
    $query->fields('country');
    $query->fields('cod');
    $query->condition('ttd.vid', '2');
    $query->condition('country.field_pais_iso2', 'KG');
    $query->orderBy('tid');

    $this->source = new MigrateSourceSQL($query, [], NULL, ['map_joinable' => FALSE]);
    $this->destination = new MigrateDestinationTerm('division_administrativa');
    $this->map = new MigrateSQLMap($this->machineName, [
      'tid' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'description' => 'ID of destination term',
      ],
    ],
    MigrateDestinationTerm::getKeySchema());

    $this->addFieldMapping('name', 'name');
    $this->addFieldMapping('format')->defaultValue('filtered_html');
    $this->addFieldMapping('weight', 'weight');
    $this->addFieldMapping('parent', 'parent')
      ->sourceMigration($this->machineName)
      ->defaultValue(0);
    $this->addFieldMapping('field_pais', 'field_pais_iso2');
    $this->addFieldMapping('field_codigo_division_admin', 'field_codigo_division_admin_value');
  }

}
