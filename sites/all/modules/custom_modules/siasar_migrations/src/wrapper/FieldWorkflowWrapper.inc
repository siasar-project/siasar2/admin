<?php

/**
 * Wrapper for import workflow fields on migrate.
 *
 * This field has no compatibility with multi-value process.
 */
class FieldWorkflowWrapper extends FieldBaseWrapper {

  /**
   * {@inheritDoc}
   */
  public function __construct($fieldName) {
    parent::__construct($fieldName);

    $this->field_type = 'workflow';
  }

  /**
   * {@inheritDoc}
   */
  public function addTables($query) {
    $query->join(
      $this->baseTableName . $this->fieldName,
      $this->tableAlias,
      $this->getJoinCondition($query)
    );
  }

  /**
   * {@inheritDoc}
   */
  public function addFields($query) {
    $query->fields($this->tableAlias, [$this->fieldName . '_value']);
  }

  /**
   * {@inheritDoc}
   */
  public function addFieldMapping() {
    $this->migration->addFieldMapping($this->fieldName, $this->fieldName . '_value');
  }

}
