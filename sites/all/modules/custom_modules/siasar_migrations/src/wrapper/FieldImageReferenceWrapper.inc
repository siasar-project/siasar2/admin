<?php

/**
 * Wrapper for import image reference fields on migrate.
 *
 * This field has no compatibility with multi-value process.
 */
class FieldImageReferenceWrapper extends FieldBaseWrapper {

  /**
   * {@inheritDoc}
   */
  public function __construct($fieldName) {
    parent::__construct($fieldName);

    $this->field_type = 'imagereference';
  }

  /**
   * {@inheritDoc}
   */
  public function addFields($query) {
    $query->fields($this->tableAlias, [
      $this->fieldName . '_fid',
      $this->fieldName . '_alt',
      $this->fieldName . '_title',
      $this->fieldName . '_width',
      $this->fieldName . '_height',
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function addFieldMapping() {
    $this->migration->addFieldMapping($this->fieldName, $this->fieldName . '_fid')
      ->sourceMigration('Files');
    $this->migration->addFieldMapping($this->fieldName . ':file_class')
      ->defaultValue('MigrateFileFid');
  }

}
