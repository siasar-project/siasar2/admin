<?php

/**
 * Base migration class for field collection items.
 */
class SiasarFieldCollectionMigrationBase extends Migration implements SiasarMigrationInterface {

  /**
   * Query.
   *
   * @var SelectQueryInterface
   */
  protected $query;

  /**
   * Field wrappers FieldBaseWrapper array.
   *
   * @var array
   */
  protected $fieldWrappers = array();

  /**
   * The field collection machine name.
   *
   * @var string
   */
  protected $fcName;

  /**
   * Country to apply conditions.
   *
   * @var string
   */
  protected $country = '';

  /**
   * Source migration name.
   *
   * @var string
   */
  protected $sourceMigration;

  public function __construct($arguments = []) {
    parent::__construct($arguments);
    // Set migration dependencies.
    $this->setDependencies();
    // Query to get all entityforms.
    $db = Database::getConnection('default', 'legacy');
    $this->query = $db->select('entityform', 'eform');
    $this->query->join('field_data_field_pais', 'country', "country.entity_id = eform.entityform_id AND country.entity_type = 'entityform'");
    $this->query->leftJoin("field_data_{$this->fcName}", $this->fcName, "{$this->fcName}.entity_id = eform.entityform_id AND {$this->fcName}.entity_type = 'entityform'");
    $this->query->join('field_collection_item', 'fc', "fc.item_id = {$this->fcName}.{$this->fcName}_value AND fc.revision_id = {$this->fcName}.{$this->fcName}_revision_id");

    // Join with field tables.
    foreach ($this->fieldWrappers as $wrapper) {
      $wrapper->addTables($this->query);
    }

    // Fields values.
    $this->query->fields('fc');
    $this->query->fields('eform', ['entityform_id', 'type']);
    // Add wrapper fields.
    foreach ($this->fieldWrappers as $wrapper) {
      $wrapper->addFields($this->query);
    }

    // Conditions.
    $this->query->condition('fc.field_name', $this->fcName);
    if (!empty($this->country)) {
      $this->query->condition('country.field_pais_iso2', $this->country);
    }

    // !!!! QUTIAR ESTO !!!!!
//    $this->query->condition('entityform_id', 175932);

    // Add wrappers conditions.
    $this->customConditions();

    // Set migration source.
    $this->source = new MigrateSourceSQL($this->query, [], NULL, ['map_joinable' => FALSE]);
    // Set migration destination.
    $this->destination = new MigrateDestinationFieldCollection($this->fcName, ['host_entity_type' => 'entityform']);

    // Set rows map.
    $this->map = new MigrateSQLMap($this->machineName, [
      'item_id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('The source ID'),
      ],
    ],
    MigrateDestinationFieldCollection::getKeySchema());

    // Add fields mapping.
    $this->addFieldMapping('archived', 'archived');
    $this->addFieldMapping('host_entity_id', 'entityform_id')->sourceMigration($this->sourceMigration);

    // Add wrapper field map.
    foreach ($this->fieldWrappers as $wrapper) {
      $wrapper->addFieldMapping($this);
    }
  }

  /**
   * Get field wrappers info.
   *
   * @return array
   *   Get field wrappers array indexed by field name and value is a array
   *   with if it's multivalued, and field type.
   */
  public function getFieldWrappersSettings() {
    $resp = [];
    /** @var FieldBaseWrapper $wrapper */
    foreach ($this->fieldWrappers as $wrapper) {
      $item = [
        'type' => $wrapper->getFieldType(),
        'multivalue' => $wrapper->isMultivalue(),
        'name' => $wrapper->getFieldName(),
      ];
      $resp[$wrapper->getFieldName()] = $item;
    }

    return $resp;
  }

  /**
   * Child class conditions.
   */
  protected function customConditions() {}

  /**
   * Set field collection name.
   *
   * @param string $name
   *   Field collection name.
   */
  protected function setFieldCollectionName($name) {
    $this->fcName = $name;
  }

  /**
   * Set country iso2 value.
   *
   * @param string $country_iso2
   */
  protected function setCountry($country_iso2) {
    $this->country = $country_iso2;
  }

  /**
   * Add a field wrapper.
   *
   * @param FieldBaseWrapper $wrapper
   */
  public function addFieldWrapper(&$wrapper) {
    $wrapper->setMigration($this);
    $this->fieldWrappers[] = $wrapper;
  }

  /**
   * Set dependencies for this migration.
   */
  protected function setDependencies() {}

  /**
   * {@inheritDoc}
   */
  public function getFieldJoinCondition($fieldName, $tableAlias, $wrapperClass, $query) {
    return $tableAlias . ".entity_id = fc.item_id AND " . $tableAlias . ".entity_type = 'field_collection_item'";
  }

  /**
   * {@inheritDoc}
   */
  public function getBundleId() {
    return $this->fcName;
  }

  /**
   * {@inheritDoc}
   */
  public function addFieldWrappers($wrappers) {
    // Update migration instance.
    /** @var FieldBaseWrapper $fieldWrapper */
    foreach ($wrappers as $fieldWrapper) {
      $this->addFieldWrapper($fieldWrapper);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getFieldWrappers() {
    return $this->fieldWrappers;
  }

}
