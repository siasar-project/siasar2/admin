<?php

/**
 * Wrapper for import date fields on migrate.
 */
class FieldDateWrapper extends FieldBaseWrapper {

  /**
   * {@inheritDoc}
   */
  public function __construct($fieldName, $multivalue = FALSE) {
    parent::__construct($fieldName, $multivalue);
    $this->field_type = 'date';
  }

  /**
   * {@inheritDoc}
   */
  public function addFields($query) {
    if (!$this->isMultivalue()) {
      $query->fields($this->tableAlias, [$this->fieldName . '_value']);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function addFieldMapping() {
    $this->migration->addFieldMapping($this->fieldName, $this->fieldName . '_value');
  }

  /**
   * {@inheritDoc}
   */
  protected function buildDatabaseValueColumn() {
    $this->databaseValueColumn = $this->fieldName . '_value';
  }

  /**
   * Save field data in her table.
   *
   * @param DatabaseConnection $connection
   *   Database connecto.
   * @param object $entity
   *   Parent entity.
   * @param string $language
   *   Language code.
   * @param array $sourceData
   *   Source field data.
   * @param string $bundle
   *   Host gield name.
   *
   * @throws Exception
   */
  public function saveFieldData($connection, $entity, $language, $sourceData, $bundle) {
    // Field collection save this field revision ok.

//    $value = NULL;
//    if (isset($sourceData[0]['value']) && $sourceData[0]['value']) {
//      $value = $sourceData[0]['value'];
//    }
//    $query = $connection->insert($this->getBaseTableName());
//    $query->fields([
//      'entity_type' => $entity->entityType(),
//      'bundle' => $bundle,
//      'deleted' => 0,
//      'entity_id' => $entity->item_id,
//      'revision_id' => $entity->revision_id,
//      'language' => $language,
//      'delta' => 0,
//      $this->fieldName . '_value' => $value,
//    ]);
//    $query->execute();
  }

}
