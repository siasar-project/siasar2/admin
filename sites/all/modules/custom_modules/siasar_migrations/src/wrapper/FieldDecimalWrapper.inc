<?php

/**
 * Wrapper for import decimal fields on migrate.
 */
class FieldDecimalWrapper extends FieldBaseWrapper {

  /**
   * {@inheritDoc}
   */
  public function __construct($fieldName, $multivalue = FALSE) {
    parent::__construct($fieldName, $multivalue);
    $this->field_type = 'decimal';
  }

  /**
   * Prepare fields.
   *
   * @param SelectQueryInterface $query
   */
  public function addFields($query) {
    if (!$this->isMultivalue()) {
      $query->fields($this->tableAlias, [$this->fieldName . '_value']);
    }
  }

  /**
   * Prepare mappings.
   *
   * @param SiasarMigrationBase $migration
   */
  public function addFieldMapping() {
    $this->migration->addFieldMapping($this->fieldName, $this->fieldName . '_value');
  }

  /**
   * {@inheritDoc}
   */
  protected function buildDatabaseValueColumn() {
    $this->databaseValueColumn = $this->fieldName . '_value';
  }

  /**
   * Save field data in her table.
   *
   * @param DatabaseConnection $connection
   *   Database connecto.
   * @param object $entity
   *   Parent entity.
   * @param string $language
   *   Language code.
   * @param array $sourceData
   *   Source field data.
   * @param string $bundle
   *   Host gield name.
   *
   * @throws Exception
   */
  public function saveFieldData($connection, $entity, $language, $sourceData, $bundle) {
    $query = $connection->insert($this->getBaseTableName());
    $query->fields([
      'entity_type' => $entity->entityType(),
      'bundle' => $bundle,
      'deleted' => 0,
      'entity_id' => $entity->item_id,
      'revision_id' => $entity->revision_id,
      'language' => $language,
      'delta' => 0,
      $this->fieldName . '_value' => $sourceData,
    ]);
    $query->execute();
  }

}
