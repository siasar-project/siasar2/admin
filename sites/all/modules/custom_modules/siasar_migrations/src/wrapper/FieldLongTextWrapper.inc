<?php

/**
 * Wrapper for import long text fields on migrate.
 *
 * This field has no compatibility with multi-value process.
 */
class FieldLongTextWrapper extends FieldBaseWrapper {

  /**
   * {@inheritDoc}
   */
  public function __construct($fieldName) {
    parent::__construct($fieldName);

    $this->field_type = 'longtext';
  }

  /**
   * {@inheritDoc}
   */
  public function addFields($query) {
    $query->fields($this->tableAlias, [$this->fieldName . '_value', $this->fieldName . '_format']);
  }

  /**
   * {@inheritDoc}
   */
  public function addDestinationSubFields(&$subfields) {
    $subfields[$this->fieldName . ':format'] = 'Text format';
  }

  /**
   * {@inheritDoc}
   */
  public function addFieldMapping() {
    $this->migration->addFieldMapping($this->fieldName, $this->fieldName . '_value');
    $this->migration->addFieldMapping($this->fieldName . ':format', $this->fieldName . '_format');
  }

  /**
   * Save field data in her table.
   *
   * @param DatabaseConnection $connection
   *   Database connecto.
   * @param object $entity
   *   Parent entity.
   * @param string $language
   *   Language code.
   * @param array $sourceData
   *   Source field data.
   * @param string $bundle
   *   Host gield name.
   *
   * @throws Exception
   */
  public function saveFieldData($connection, $entity, $language, $sourceData, $bundle) {
    $query = $connection->insert($this->getBaseTableName());
    $query->fields([
      'entity_type' => $entity->entityType(),
      'bundle' => $bundle,
      'deleted' => 0,
      'entity_id' => $entity->item_id,
      'revision_id' => $entity->revision_id,
      'language' => $language,
      'delta' => 0,
      $this->fieldName . '_value' => $sourceData[0],
      $this->fieldName . '_format' => $sourceData['arguments']['format'],
    ]);
    $query->execute();
  }

}
