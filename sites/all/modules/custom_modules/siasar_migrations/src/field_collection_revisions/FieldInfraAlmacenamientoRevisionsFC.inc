<?php

/**
 * Migration for PAT entityforms.
 */
class FieldInfraAlmacenamientoRevisionsFC extends SiasarFieldCollectionRevisionsMigrationBase {
  public function __construct($arguments = []) {
    // Copy entityform settings.
    $patArg = $arguments;
    $patArg['machine_name'] = 'FieldInfraAlmacenamiento';
    $this->setCountry('KG');
    $migParent = new FieldInfraAlmacenamientoFC($patArg);

    $this->setFieldCollectionName($migParent->getBundleId());
    $this->addFieldWrappers($migParent->getFieldWrappers());

    // Prepare migration.
    $this->description = "Migration of '{$this->entityFormMachineName}' Entityform submissions revisions from SIASAR database";
    $this->dependencies = ['FieldInfraAlmacenamiento'];
    // Set source migration to get host entity.
    $this->sourceMigration = [
      'fc_source' => 'FieldInfraAlmacenamiento',
      'ef_source' => 'System',
      'ef_rev_source' => 'SystemRevisions',
    ];
    parent::__construct($arguments);
  }

  /**
   * Child class conditions.
   */
  protected function customConditions() {
    // Custom conditions.
//    $this->query->condition(FieldBaseWrapper::getDefaultTableAlias('field_pais') . '.' . 'field_pais_iso2', 'KG');
  }

  /**
   * Child class conditions.
   */
  protected function customCountConditions() {
    $countryField = new FieldCountryWrapper('field_pais');
    $countryField->setMigration($this);
    $countryField->setBaseTableName('field_revision_');
    $countryField->addTables($this->count_query);
    // Custom conditions.
    $this->count_query->condition(FieldBaseWrapper::getDefaultTableAlias('field_pais') . '.' . 'field_pais_iso2', 'KG');
  }

}
