<?php

/**
 * Migration for PAT entityforms.
 */
class PATRevisionsMigration extends SiasarRevisionsMigrationBase {
  public function __construct($arguments = []) {
    // Copy entityform settings.
    $patArg = $arguments;
    $patArg['machine_name'] = 'PAT';
    $this->parentMigration = new PATMigration($patArg);

    $this->setBundleId($this->parentMigration->getBundleId());
    $this->addFieldWrappers($this->parentMigration->getFieldWrappers());

    // Prepare migration.
    $this->description = "Migration of '{$this->entityFormMachineName}' Entityform submissions revisions from SIASAR database";
    $this->dependencies = ['PAT'];
    $this->sourceMigration = 'PAT';
    parent::__construct($arguments);
  }

  /**
   * Child class conditions.
   */
  protected function customConditions() {
    // Custom conditions.
    $this->query->condition(FieldBaseWrapper::getDefaultTableAlias('field_pais') . '.' . 'field_pais_iso2', 'KG');
  }

  /**
   * Child class conditions.
   */
  protected function customCountConditions() {
    $countryField = new FieldCountryWrapper('field_pais');
    $countryField->setMigration($this);
    $countryField->setBaseTableName('field_revision_');
    $countryField->addTables($this->count_query);
    // Custom conditions.
    $this->count_query->condition(FieldBaseWrapper::getDefaultTableAlias('field_pais') . '.' . 'field_pais_iso2', 'KG');
  }

}
