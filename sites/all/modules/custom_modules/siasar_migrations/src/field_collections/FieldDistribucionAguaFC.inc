<?php

/**
 * Migration class for field_f_red_de_distribucion field collection.
 */
class FieldDistribucionAguaFC extends SiasarFieldCollectionMigrationBase {
  public function __construct($arguments = []) {
    // Set field collection name.
    $this->setFieldCollectionName('field_f_red_de_distribucion');
    // Set country to filter.
    $this->setCountry('KG');
    // Set source migration to get host entity.
    $this->sourceMigration = 'System';

    // Text fields.
    $this->addFieldWrapper(new FieldTextWrapper('field_f1_1'));

    $this->addFieldWrapper(new FieldLongTextWrapper('field_f3_2'));

    // Number fields.
    $this->addFieldWrapper(new FieldDecimalWrapper('field_f1_5'));

    $this->addFieldWrapper(new FieldIntegerWrapper('field_f1_2'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_f1_3'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_f1_4'));

    // Lists fields.
    $this->addFieldWrapper(new FieldTextListWrapper('field_f2_1'));

    // Reference fields.
    $this->addFieldWrapper(new FieldTermReferenceWrapper('field_edo_fisico_red_dist'));

    parent::__construct($arguments);
  }

  /**
   * {@inheritDoc}
   */
  protected function setDependencies() {
    $this->dependencies = ['System'];
  }

}
