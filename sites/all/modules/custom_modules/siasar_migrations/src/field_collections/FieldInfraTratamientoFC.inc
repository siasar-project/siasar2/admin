<?php

/**
 * Migration class for field_d_infraestructura field collection.
 */
class FieldInfraTratamientoFC extends SiasarFieldCollectionMigrationBase {
  public function __construct($arguments = []) {
    // Set field collection name.
    $this->setFieldCollectionName('field_d_infraestructura');
    // Set country to filter.
    $this->setCountry('KG');
    // Set source migration to get host entity.
    $this->sourceMigration = 'System';

    // Text fields.
    $this->addFieldWrapper(new FieldTextWrapper('field_d1_1'));

    $this->addFieldWrapper(new FieldLongTextWrapper('field_d2_2'));

    // Reference fields.
    $this->addFieldWrapper(new FieldTermReferenceWrapper('field_tipo_de_tratamiento'));
    $this->addFieldWrapper(new FieldTermReferenceWrapper('field_edo_fisico_linea_tto'));

    // Boolean fields.
    $this->addFieldWrapper(new FieldBooleanWrapper('field_d1_3'));

    // Geofield fields.
    $this->addFieldWrapper(new FieldGeofieldWrapper('field_d1_4'));

    // Number fields.
    $this->addFieldWrapper(new FieldDecimalWrapper('field_d1_5'));

    parent::__construct($arguments);
  }

  /**
   * {@inheritDoc}
   */
  protected function setDependencies() {
    $this->dependencies = ['System'];
  }

}
