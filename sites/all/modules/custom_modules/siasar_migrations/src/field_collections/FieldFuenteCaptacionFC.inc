<?php

/**
 * Migration class for field_fuente_y_o_captacion field collection.
 */
class FieldFuenteCaptacionFC extends SiasarFieldCollectionMigrationBase {
  public function __construct($arguments = []) {
    // Set field collection name.
    $this->setFieldCollectionName('field_fuente_y_o_captacion');
    // Set country to filter.
    $this->setCountry('KG');
    // Set source migration to get host entity.
    $this->sourceMigration = 'System';

    // Text fields.
    $this->addFieldWrapper(new FieldTextWrapper('field_nombre_de_la_fuente'));
    $this->addFieldWrapper(new FieldTextWrapper('field_codigo_de_la_fuente'));

    $this->addFieldWrapper(new FieldLongTextWrapper('field_observaciones'));

    // Number fields.
    $this->addFieldWrapper(new FieldDecimalWrapper('field_b1_altitud'));

    // Lists fields.
    $this->addFieldWrapper(new FieldTextListWrapper('field_existe_macro_medicion'));

    // Reference fields.
    $this->addFieldWrapper(new FieldTermReferenceWrapper('field_tipo_de_fuente'));
    $this->addFieldWrapper(new FieldTermReferenceWrapper('field_edo_fisico_infra_cptcion'));

    // Boolean fields.
    $this->addFieldWrapper(new FieldBooleanWrapper('field_es_la_fuente_principal'));
    $this->addFieldWrapper(new FieldBooleanWrapper('field_existencia_deareas_verdes'));
    $this->addFieldWrapper(new FieldBooleanWrapper('field_existencia_z_erosionadas'));
    $this->addFieldWrapper(new FieldBooleanWrapper('field_proteccion_de_la_fuente'));
    $this->addFieldWrapper(new FieldBooleanWrapper('field_existenciade_contaminacion'));
    $this->addFieldWrapper(new FieldBooleanWrapper('field_existencia_de_indicio'));
    $this->addFieldWrapper(new FieldBooleanWrapper('field_infraestructura_captacion'));

    // Measures fields.
    $this->addFieldWrapper(new FieldMeasureWrapper('field_caudal_de_la_fuente'));
    $this->addFieldWrapper(new FieldMeasureWrapper('field_caudal_fuente_epoca_seca'));

    // Date fields.
    $this->addFieldWrapper(new FieldDateWrapper('field_fecha_toma_caudal_ep_seca'));
    $this->addFieldWrapper(new FieldDateWrapper('field_fecha_toma_cdal_epoca_seca'));

    // Geofield fields.
    $this->addFieldWrapper(new FieldGeofieldWrapper('field_b1_7'));

    parent::__construct($arguments);
  }

  /**
   * {@inheritDoc}
   */
  protected function setDependencies() {
    $this->dependencies = ['System'];
  }

}
