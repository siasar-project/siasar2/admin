<?php

/**
 * Migration class for field_com_fc_systypimprove field collection.
 */
class FieldSistemaSaneamientoFC extends SiasarFieldCollectionMigrationBase {
  public function __construct($arguments = []) {
    // Set field collection name.
    $this->setFieldCollectionName('field_com_fc_systypimprove');
    // Set country to filter.
    $this->setCountry('KG');
    // Set source migration to get host entity.
    $this->sourceMigration = 'Community';

    // Text fields.
    $this->addFieldWrapper(new FieldTextWrapper('field_com_e1source'));

    // Reference fields.
    $this->addFieldWrapper(new FieldTermReferenceWrapper('field_com_e1estado'));

    parent::__construct($arguments);
  }

  /**
   * {@inheritDoc}
   */
  protected function setDependencies() {
    $this->dependencies = ['Community'];
  }

}
