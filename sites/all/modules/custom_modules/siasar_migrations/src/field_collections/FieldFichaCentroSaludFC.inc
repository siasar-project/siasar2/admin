<?php

/**
 * Migration class for field_com_fic_cs_ficha field collection.
 */
class FieldFichaCentroSaludFC extends SiasarFieldCollectionMigrationBase {
  public function __construct($arguments = []) {
    // Set field collection name.
    $this->setFieldCollectionName('field_com_fic_cs_ficha');
    // Set country to filter.
    $this->setCountry('KG');
    // Set source migration to get host entity.
    $this->sourceMigration = 'Community';

    // Text fields.
    $this->addFieldWrapper(new FieldTextWrapper('field_group_com_fic_cscgnom'));
    $this->addFieldWrapper(new FieldTextWrapper('field_com_fic_cssgcodigo'));

    $this->addFieldWrapper(new FieldLongTextWrapper('field_com_fic_cssa_pd'));

    // Number fields.
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cscgperm'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cscgperf'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cscgusum'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cscgusuf'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pa'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pe'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pi'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pm'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pq'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pu'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_paa'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pba'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pca'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pf'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pj'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pn'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pr'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pv'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pab'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pbb'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pcb'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pc'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pg'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pk'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_po'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_ps'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pw'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pac'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pbc'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pcc'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pd'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_ph'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pl'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pp'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pt'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_px'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pad'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pbd'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cssh_pcd'));

    // Reference fields.
    $wrapper = new FieldEntityReferenceWrapper('field_system_ref');
    $wrapper->setSubmigration('', 'System');
    $this->addFieldWrapper($wrapper);

    // List fields.
    $this->addFieldWrapper(new FieldTextListWrapper('field_com_fic_cssa_pa'));

    parent::__construct($arguments);
  }

  /**
   * {@inheritDoc}
   */
  protected function setDependencies() {
    $this->dependencies = ['Community', 'System'];
  }

}
