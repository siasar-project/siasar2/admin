<?php

/**
 * Migration class for field_lineas_de_conduccion field collection.
 */
class FieldLineasConduccionFC extends SiasarFieldCollectionMigrationBase {
  public function __construct($arguments = []) {
    // Set field collection name.
    $this->setFieldCollectionName('field_lineas_de_conduccion');
    // Set country to filter.
    $this->setCountry('KG');
    // Set source migration to get host entity.
    $this->sourceMigration = 'System';

    // Text fields.
    $this->addFieldWrapper(new FieldTextWrapper('field_c1_1'));

    $this->addFieldWrapper(new FieldLongTextWrapper('field_c2_2'));

    // Reference fields.
    $this->addFieldWrapper(new FieldTermReferenceWrapper('field_edo_fisico_linea_conducion'));

    // Boolean fields.
    $this->addFieldWrapper(new FieldBooleanWrapper('field_c1_4'));

    // Measures fields.
    $this->addFieldWrapper(new FieldMeasureWrapper('field_longitud_de_la_linea'));
    $this->addFieldWrapper(new FieldMeasureWrapper('field_diametro_medio_de_tuberia'));

    parent::__construct($arguments);
  }

  /**
   * {@inheritDoc}
   */
  protected function setDependencies() {
    $this->dependencies = ['System'];
  }

}
