<?php

/**
 * Migration class for field_tecnicos field collection.
 */
class FieldTecnicosFC extends SiasarFieldCollectionMigrationBase {
  public function __construct($arguments = []) {
    // Set field collection name.
    $this->setFieldCollectionName('field_tecnicos');
    // Set country to filter.
    $this->setCountry('KG');
    // Set source migration to get host entity.
    $this->sourceMigration = 'ServiceProvider';

    // Text fields.
    $this->addFieldWrapper(new FieldTextWrapper('field_nombre_tecnico'));
    $this->addFieldWrapper(new FieldTextWrapper('field_telef'));

    // Reference fields.
    $this->addFieldWrapper(new FieldTermReferenceWrapper('field_genero_tecnico'));
    $this->addFieldWrapper(new FieldEntityReferenceWrapper('field_cargo_tecnico'));

    parent::__construct($arguments);
  }

  /**
   * {@inheritDoc}
   */
  protected function setDependencies() {
    $this->dependencies = ['ServiceProvider'];
  }

}
