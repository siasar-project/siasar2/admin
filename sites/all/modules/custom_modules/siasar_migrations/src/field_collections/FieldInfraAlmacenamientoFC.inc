<?php

/**
 * Migration class for field_e_infraestructura_almac field collection.
 */
class FieldInfraAlmacenamientoFC extends SiasarFieldCollectionMigrationBase {
  public function __construct($arguments = []) {
    // Set field collection name.
    $this->setFieldCollectionName('field_e_infraestructura_almac');
    // Set country to filter.
    $this->setCountry('KG');
    // Set source migration to get host entity.
    $this->sourceMigration = 'System';

    // Text fields.
    $this->addFieldWrapper(new FieldTextWrapper('field_e1_1'));

    $this->addFieldWrapper(new FieldLongTextWrapper('field_e2_2'));

    // Measures fields.
    $this->addFieldWrapper(new FieldMeasureWrapper('field_capacidad_almacenamiento'));
    $this->addFieldWrapper(new FieldMeasureWrapper('field_frecuencia_limpieza_infr'));

    // Geofield fields.
    $this->addFieldWrapper(new FieldGeofieldWrapper('field_latitud_longitud'));

    // Number fields.
    $this->addFieldWrapper(new FieldDecimalWrapper('field_e1_5_altitud'));

    // Reference fields.
    $this->addFieldWrapper(new FieldTermReferenceWrapper('field_edo_fisico_infr_almac'));

    parent::__construct($arguments);
  }

  /**
   * {@inheritDoc}
   */
  protected function setDependencies() {
    $this->dependencies = ['System'];
  }

}
