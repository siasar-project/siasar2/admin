<?php

/**
 * Migration class for field_con_fic_ce_ficha field collection.
 */
class FieldCentrosEducativosFC extends SiasarFieldCollectionMigrationBase {
  public function __construct($arguments = []) {
    // Set field collection name.
    $this->setFieldCollectionName('field_con_fic_ce_ficha');
    // Set country to filter.
    $this->setCountry('KG');
    // Set source migration to get host entity.
    $this->sourceMigration = 'Community';

    // Number fields.
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pa'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pe'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pi'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pm'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pq'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pu'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_paa'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pba'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pca'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pf'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pj'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pn'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pr'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pv'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pab'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pb'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pbb'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pcb'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pc'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pg'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pk'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_po'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_ps'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pw'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pac'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pbc'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pcc'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pd'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_ph'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pl'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pp'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pt'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_px'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pad'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pbd'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_cesh_pcd'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_ce_fdocf'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_ce_fdocm'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_ce_festf'));
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_fic_ce_festm'));

    // Text fields.
    $this->addFieldWrapper(new FieldTextWrapper('field_con_fic_ce_fnombre'));
    $this->addFieldWrapper(new FieldTextWrapper('field_com_fic_ce_fcodigo'));

    $this->addFieldWrapper(new FieldLongTextWrapper('field_com_fic_cesa_pd'));

    // Lists fields.
    $this->addFieldWrapper(new FieldTextListWrapper('field_com_fic_cesa_pa'));

    // Reference fields.
    $wrapper = new FieldEntityReferenceWrapper('field_system_ref');
    $wrapper->setSubmigration('', 'System');
    $this->addFieldWrapper($wrapper);

    parent::__construct($arguments);
  }

  /**
   * {@inheritDoc}
   */
  protected function setDependencies() {
    $this->dependencies = ['Community', 'System'];
  }

}
