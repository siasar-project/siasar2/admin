<?php

/**
 * Migration class for field_rehabilitaciones_y_o_ampli field collection.
 */
class FieldAmpliacionesFC extends SiasarFieldCollectionMigrationBase {
  public function __construct($arguments = []) {
    // Set field collection name.
    $this->setFieldCollectionName('field_rehabilitaciones_y_o_ampli');
    // Set country to filter.
    $this->setCountry('KG');
    // Set source migration to get host entity.
    $this->sourceMigration = 'System';

    // Reference fields.
    $this->addFieldWrapper(new FieldTermReferenceWrapper('field_tipo_de_rehabilitacion'));

    $this->addFieldWrapper(new FieldEntityReferenceWrapper('field_origen_de_financiamiento_i'));
    $this->addFieldWrapper(new FieldEntityReferenceWrapper('field_programa_especifico_fondos'));
    $this->addFieldWrapper(new FieldEntityReferenceWrapper('field_institucion_ejecutora'));

    // Measures fields.
    $this->addFieldWrapper(new FieldMeasureWrapper('field_monto_rehabilitacion'));

    // Date fields.
    $this->addFieldWrapper(new FieldDateWrapper('field_ano'));

    parent::__construct($arguments);
  }

  /**
   * {@inheritDoc}
   */
  protected function setDependencies() {
    $this->dependencies = ['System'];
  }

}
