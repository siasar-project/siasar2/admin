<?php

/**
 * Migration class for field_com_viv_con_sistema field collection.
 */
class FieldViviendasConSistemaFC extends SiasarFieldCollectionMigrationBase {
  public function __construct($arguments = []) {
    // Set field collection name.
    $this->setFieldCollectionName('field_com_viv_con_sistema');
    // Set country to filter.
    $this->setCountry('KG');
    // Set source migration to get host entity.
    $this->sourceMigration = 'Community';

    // Number fields.
    $this->addFieldWrapper(new FieldIntegerWrapper('field_com_viv_con_sistnva'));

    // Reference fields.
    $wrapper = new FieldEntityReferenceWrapper('field_system_ref');
    $wrapper->setSubmigration('', 'System');
    $this->addFieldWrapper($wrapper);

    $wrapper = new FieldEntityReferenceWrapper('field_prestador_servicio');
    $wrapper->setSubmigration('', 'ServiceProvider');
    $this->addFieldWrapper($wrapper);

    parent::__construct($arguments);
  }

  /**
   * {@inheritDoc}
   */
  protected function setDependencies() {
    $this->dependencies = ['Community', 'System', 'ServiceProvider'];
  }

}
