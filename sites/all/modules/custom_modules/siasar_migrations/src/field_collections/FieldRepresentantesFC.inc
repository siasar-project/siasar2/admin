<?php

/**
 * Migration class for field_representantes field collection.
 */
class FieldRepresentantesFC extends SiasarFieldCollectionMigrationBase {
  public function __construct($arguments = []) {
    // Set field collection name.
    $this->setFieldCollectionName('field_representantes');
    // Set country to filter.
    $this->setCountry('KG');
    // Set source migration to get host entity.
    $this->sourceMigration = 'ServiceProvider';

    // Text fields.
    $this->addFieldWrapper(new FieldTextWrapper('field_nombre_representante'));
    $this->addFieldWrapper(new FieldTextWrapper('field_telefon'));

    // Reference fields.
    $this->addFieldWrapper(new FieldTermReferenceWrapper('field_genero'));
    $this->addFieldWrapper(new FieldEntityReferenceWrapper('field_cargo_representante'));

    parent::__construct($arguments);
  }

  /**
   * {@inheritDoc}
   */
  protected function setDependencies() {
    $this->dependencies = ['ServiceProvider'];
  }

}
