<?php

/**
 * Migration for PAT entityforms.
 */
class WaterQuality extends Migration {
  public function __construct($arguments = []) {
    parent::__construct($arguments);

    // Query to get all entityforms.
    $db = Database::getConnection('default', 'legacy');
    $query = $db->select('entityform', 'eform');
    $query->join('field_data_field_pais', 'country', "country.entity_id = eform.entityform_id AND country.entity_type = 'entityform'");
    $query->fields('eform');
    $query->fields('country');
    $query->condition('eform.type', 'waq');
    $query->condition('country.field_pais_iso2', 'KG');

    // Set migration source.
    $this->source = new MigrateSourceSQL($query, [], NULL, ['map_joinable' => FALSE]);
    // Set migration destination.
    $this->destination = new MigrateDestinationEntityAPI('entityform', 'waq');

    // Set rows map.
    $this->map = new MigrateSQLMap($this->machineName, [
      'entityform_id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('The source ID'),
      ],
      'vid' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('The current entityform revision'),
      ],
    ],
    MigrateDestinationEntityAPI::getKeySchema('entityform'));

    // Add fields mapping.
    $this->addFieldMapping('created', 'created');
  }

}
