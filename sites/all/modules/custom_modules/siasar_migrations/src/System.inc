<?php

/**
 * Migration for System entityforms.
 */
class System extends SiasarMigrationBase {
  public function __construct($arguments = []) {

    // Set entity form type ID.
    $this->setBundleId('sistema');

    // Create field maps.
    $this->addFieldWrappers([
      new FieldDateWrapper('field_fecha_de_aplicaci_n'),
      new FieldEntityReferenceWrapper('field_user_reference'),
      new FieldWorkflowWrapper('field_status'),
      new FieldCountryWrapper('field_pais'),
      new FieldImageReferenceWrapper('field_imagen'),
      new FieldTextWrapper('field_entity_name'),
      new FieldDateWrapper('field_anno_construccion'),
      new FieldGeofieldWrapper('field_latitud'),
      new FieldDecimalWrapper('field_alto'),
      new FieldTextWrapper('field_codigo_de_sistema'),
      new FieldEntityReferenceWrapper('field_cuenca_hidrografica'),
      new FieldEntityReferenceWrapper('field_area_planificacion'),
      new FieldEntityReferenceWrapper('field_otras_divisiones'),
      new FieldTermReferenceWrapper('field_tipo_de_sistema_de_absto', TRUE),
      new FieldTextWrapper('field_a4_2'),
      new FieldBooleanWrapper('field_agua_en_verano'),
      new FieldBooleanWrapper('field_agua_en_invierno'),
      new FieldImageReferenceWrapper('field_a6_croquis'),
      new FieldMeasureWrapper('field_caudal_actual_del_sistema'),
      new FieldTextListWrapper('field_g2_1'),
      new FieldTextListWrapper('field_g3_1'),
      new FieldMeasureWrapper('field_cantidad_de_cloro_residual'),
      new FieldDateWrapper('field_fecha_analisiscalidad_agua'),
      new FieldBooleanWrapper('field_g4_3'),
      new FieldDateWrapper('field_fecha_analisis_coliformes'),
      new FieldBooleanWrapper('field_g4_4'),
      new FieldDateWrapper('field_fecha_analisis_fq'),
      new FieldLongTextWrapper('field_h1_1'),
      new FieldEntityReferenceWrapper('field_rating_s1'),
      new FieldTextWrapper('field_data_source'),
      new FieldIntegerWrapper('field_closed_revisions', TRUE),
    ]);

    $local_entity_field = new FieldTermReferenceWrapper('field_entidad_local');
    $local_entity_field->setSubmigration('tid', 'AdministrativeDivision');

    $this->addFieldWrapper($local_entity_field);

    // Prepare migration.
    $this->dependencies = ['Files', 'AdministrativeDivision', 'Users'];
    parent::__construct($arguments);
  }

  /**
   * Child class conditions.
   */
  protected function customConditions() {
    // Custom conditions.
    $this->query->condition(FieldBaseWrapper::getDefaultTableAlias('field_pais') . '.' . 'field_pais_iso2', 'KG');
  }

  /**
   * Child class conditions.
   */
  protected function customCountConditions() {
    $countryField = new FieldCountryWrapper('field_pais');
    $countryField->setMigration($this);
    $countryField->addTables($this->count_query);
    // Custom conditions.
    $this->count_query->condition(FieldBaseWrapper::getDefaultTableAlias('field_pais') . '.' . 'field_pais_iso2', 'KG');
  }

}
