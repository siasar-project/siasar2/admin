<?php

/**
 * Migration base for Siasar entityforms.
 */
class FilesMigration extends Migration implements SiasarMigrationInterface {

  /**
   * Query.
   *
   * @var SelectQueryInterface
   */
  protected $query;
  protected $count_query;

  /**
   * Field wrappers FieldBaseWrapper array.
   *
   * @var array
   */
  protected $fieldWrappers = array();

  /**
   * The entity form type machine name.
   *
   * @var string
   */
  protected $bundleMachineName;

  public function __construct($arguments = []) {
    parent::__construct($arguments);

    if (empty($arguments['destination_dir'])) {
      $arguments['destination_dir'] = 'public://';
    }
    if (empty($arguments['source_dir'])) {
      $arguments['source_dir'] = 'sites/default/files/legacy/';
    }

    $db = Database::getConnection('default', 'legacy');
    $this->query = $db->select('file_managed', 'file');
    $this->query->distinct();
    $this->query->fields('file');

    // Filtered referenced files.
    $this->query->leftJoin('field_revision_field_imagenc', 'imagenc', 'file.fid = imagenc.field_imagenc_fid');
    $this->query->leftJoin('field_revision_field_imagenps', 'imagenps', 'file.fid = imagenps.field_imagenps_fid');
    $this->query->leftJoin('field_revision_field_imagenpat', 'imagenpat', 'file.fid = imagenpat.field_imagenpat_fid');
    $this->query->leftJoin('field_revision_field_imagen', 'imagen', 'file.fid = imagen.field_imagen_fid');
    $this->query->leftJoin('field_revision_field_a6_croquis', 'croquis', 'file.fid = croquis.field_a6_croquis_fid');

    $this->query->leftJoin('entityform_revision', 'eform', '(imagenc.revision_id = eform.vid) OR (imagenps.revision_id = eform.vid) OR (imagenpat.revision_id = eform.vid) OR (imagen.revision_id = eform.vid) OR (croquis.revision_id = eform.vid)');
    $this->query->join('field_revision_field_pais','fieldpais',"fieldpais.entity_id = eform.entityform_id AND fieldpais.entity_type = 'entityform'");

    $this->query->condition('fieldpais.field_pais_iso2', 'KG');

    $sourceFields = [
      'uid' => 'Creator ID',
      'status' => 'File status',
      'timestamp' => 'Creation time',
    ];

//    $this->count_query = Database::getConnection('default', 'legacy')->select('file_managed', 'file');
//    $this->count_query->fields('file', ['COUNT(*)']);

    // Quitar esto !!
    $this->count_query = NULL;

    $this->source = new MigrateSourceSQL($this->query, $sourceFields, $this->count_query, ['map_joinable' => FALSE]);
    $this->destination = new MigrateDestinationFile('file', 'MigrateFileUri', ['file_replace' => FILE_EXISTS_RENAME]);

    if (!isset($this->map)) {
      $this->map = new MigrateSQLMap($this->machineName,
        [
          'fid' => [
            'type' => 'int',
            'unsigned' => TRUE,
            'not null' => TRUE,
            'description' => 'Source file ID',
            'alias' => 'f',
          ],
        ],
        MigrateDestinationFile::getKeySchema()
      );
    }

    // Set dependencies.
    $this->dependencies = ['Users'];

    // Setup common mappings
    $this->addFieldMapping('destination_dir')->defaultValue($arguments['destination_dir']);
    $this->addFieldMapping('source_dir')->defaultValue($arguments['source_dir']);
    $this->addFieldMapping('file_replace')->defaultValue(MigrateFile::FILE_EXISTS_REUSE);
    $this->addFieldmapping('preserve_files')->defaultValue(TRUE);
    $this->addFieldMapping('uid', 'uid');
    $this->addFieldMapping('timestamp', 'timestamp');
    $this->addFieldMapping('value', 'uri');
    $this->addFieldMapping('destination_file', 'filename');

    $this->addUnmigratedSources(['filename', 'filemime', 'filesize', 'type', 'status']);
    $this->addUnmigratedDestinations(['path', 'urlencode']);

  }

  /**
   * Get field wrappers info.
   *
   * @return array
   *   Get field wrappers array indexed by field name and value is a array
   *   with if it's multivalued, and field type.
   */
  public function getFieldWrappersSettings() {
    $resp = [];
    /** @var FieldBaseWrapper $wrapper */
    foreach ($this->fieldWrappers as $wrapper) {
      $item = [
        'type' => $wrapper->getFieldType(),
        'multivalue' => $wrapper->isMultivalue(),
        'name' => $wrapper->getFieldName(),
      ];
      $resp[$wrapper->getFieldName()] = $item;
    }

    return $resp;
  }

  /**
   * Prepare rows.
   */
  public function prepareRow($row) {
    // Prepare file path to add to base source path.
    $row->uri = str_replace('public://', '', $row->uri);

    // Process UID field.
    if (!empty($row->uid)) {
      $query = db_select('migrate_map_users', 'map');
      $query->fields('map', ['destid1']);
      $query->condition('sourceid1', $row->uid);
      $query->isNotNull('destid1');

      $results = $query->execute()->fetchCol();

      if (!empty($results)) {
        // Assign the new uid.
        $row->uid = current($results);
      }
    }

    return parent::prepareRow($row);
  }

  /**
   * Child class conditions.
   */
  protected function customConditions() {}

  /**
   * Child class conditions.
   */
  protected function customCountConditions() {}

  /**
   * Set entity form type ID.
   *
   * @param string $id
   *   The entity form ID.
   */
  protected function setBundleId($id) {
    $this->bundleMachineName = $id;
  }

  /**
   * The entity form ID.
   *
   * @return string
   *   The entity form ID.
   */
  public function getBundleId() {
    return $this->bundleMachineName;
  }

  /**
   * Add a field wrapper.
   *
   * @param FieldBaseWrapper $wrapper
   */
  public function addFieldWrapper(&$wrapper) {
    $wrapper->setMigration($this);
    $this->fieldWrappers[] = $wrapper;
  }

  public function getFieldWrappers() {
    return $this->fieldWrappers;
  }

  /**
   *
   * @param type $wrappers
   */
  public function addFieldWrappers($wrappers) {
    // Update migration instance.
    /** @var FieldBaseWrapper $fieldWrapper */
    foreach ($wrappers as $fieldWrapper) {
      $this->addFieldWrapper($fieldWrapper);
    }
  }

  public function getFieldJoinCondition($fieldName, $tableAlias, $wrapperClass, $query) {
    return '';
  }

  public function complete($entityForm, $row) {

  }

}
